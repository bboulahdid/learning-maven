# Learning Maven

Maven is a build automation tool : take a Java project and construct some deliverable we can use or deploy (e.g. automate the process of creating a `.jar` or `.war` file from a java project)

It emphasizes convention over configuration

Have some nice features like _dependency management_

It's underneath the Apache Software Foundation umbrella

Created in 2003

## Introduction

### Maven overview

Build tool :

* Creates deployable artifacts from source code
* Automated / repeatable builds (compile code source, run tests...)
* Deploying artifacts on servers
* IDE independence
* Integration with other build tools

Dependency management :

* Download project dependencies from centralized repositories
* Automatically resolve the libraries required by project dependencies (transitive dependencies)
* Dependency scoping (conditionally include a dependency)

Project management :

* Artifacts versioning
* Change logs
* Documentation / Javadocs / Reports

Standardized approach to building software :

* Uniformity across projects through patterns
* Convention over configuration (projects follow the same processes, the same layout...)
* Consistent path for all projects

Command line tool / IDE integration

### Maven landscape

Project Object Model (POM) :

* Describes, configures and customizes a Maven Project
* Maven reads the `pom.xml`file to build a project
* Defines the "address" for the project artifact using a coordinate system (`artifactId` , `groupId`, `version`)
* Specifies project information, plugins, goals, dependencies and profiles

Repositories :

* Holds build artifacts and dependencies of varying types
* Local repositories (local cache)
* Remote repositories
* Local repository takes precedence during dependency resolution

Plugins and goals :

> You can look at a `plugin` as a class and `goals` as methods / actions to perform the maven builds

* Plugin is a collection of goals (e.g. compiler plugin has 2 goals: compile source, compile test)
* Goals perform the actions in Maven builds
* All work in Maven is done via plugins and goals
* Called independently or as part of a lifecycle phase

Lifecycle and phases :

[Introduction to the Build Lifecycle](https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html)

[Maven Lifecycles Reference](http://maven.apache.org/ref/3.5.0/maven-core/lifecycles.html)

* Lifecycle is a sequence of named phases
* phases are executed sequentially
* Maven has 3 lifecycles :
    * `clean`,
    * `default` (the largest lifecycle with 23 phases),
    * `site`
* Executing a phase executes all previous phases

> Plugins bind to different phases in a lifecycle

### Maven technical overview :

![Maven technical overview](./images/maven-tech-overview.png)

> Here we have 3 phases from the `default` lifecycle

## Maven Standard Directory Layout

[Maven Standard Directory Layout](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html)

## Maven commands :

* `mvn compile`: Execute the `compile` phase that compiles the code source
* `mvn site`: Execute the `site` phase from the `site` lifecycle that generates a website with the project information
* `mvn help:effective-pom`: Call the `effective-pom` goal on the `help` plugin to see the entire `pom.xml` with the inherited configuration
* `mvn archetype:generate`: Generate a Maven project based on an archetype
    * [Introduction to Archetypes](https://maven.apache.org/guides/introduction/introduction-to-archetypes.html)
* `mvn eclipse:eclipse`: Turn a maven project to an eclipse project
* `mvn dependency:copy-dependencies`: Run the `copy-dependencies` goal on the `dependency` plugin to poll down the dependencies for the project

## Dependency Management :

[Maven Central Repository](http://search.maven.org/)

> Polling in the  different libraries needed for the project from a local / remote repository

### Transitive Dependency :

A dependency of a dependency (Maven manage transitive dependencies like a charm)

### Remote Repository :

The default repository in Maven is `Central Maven Repository`.

To change the default settings, there the `~/.m2/settings.xml` where we can specify new settings : [settings.xml](https://maven.apache.org/settings.html)

### Dependency scope :

[Dependency Mechanism](https://maven.apache.org/guides/introduction/introduction-to-dependency-mechanism.html)

### Conflict Resolution :

[Dependency Exclusions](https://maven.apache.org/guides/introduction/introduction-to-optional-and-excludes-dependencies.html)

[Solving dependency conflicts in Maven](https://www.ricston.com/blog/solving-dependency-conflicts-maven/)


